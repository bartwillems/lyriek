# Lyriek

A multi-threaded GTK 3 application for fetching the lyrics of the current playing song.

![lyriek window](screenshots/lyriek-window.png)
